import React from 'react'

import Link from 'gatsby-link'


const SixthPage = () => (
  
<div>
    
<h1>Hi from the second page</h1>
    
<p>Welcome to page 6</p>
    
<Link to="/page-7/">Go page 7</Link>
  
</div>


)

export default SixthPage
