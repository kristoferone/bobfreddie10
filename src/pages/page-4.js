import React from 'react'

import Link from 'gatsby-link'


const FourthPage = () => (
  
<div>
    
<h1>Hi from the fourth page</h1>
    
<p>Welcome to page 4</p>
    
<Link to="/page-5/">Go page 5</Link>
  
</div>


)


export default FourthPage