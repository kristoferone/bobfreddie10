import React from 'react'

import Link from 'gatsby-link'


const FifthPage = () => (
  
<div>
    
<h1>Hi from the fifth page</h1>
    
<p>Welcome to page 5</p>
    
<Link to="/page-6/">Go page 6</Link>
  
</div>


)



export default FifthPage
