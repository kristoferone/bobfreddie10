import React from 'react'

import Link from 'gatsby-link'


const SeventhPage = () => (
  
<div>
    
<h1>Hi from the seventh page</h1>
    
<p>Welcome to page 2</p>
    
<Link to="/page-8/">Go page 8</Link>
  
</div>


)

export default SeventhPage
