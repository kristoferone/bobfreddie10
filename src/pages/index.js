import React from 'react'
import Link from 'gatsby-link'

const IndexPage = () => (
  <div>
    <h1>Hello Jamie</h1>
    <p>Welcome to your online journal.</p>
    <p>You can write a little bit everyday.</p>
    <p>And learn to code.</p>
    <Link to="/page-2/">Go to page 2</Link>
  </div>
 

)

export default IndexPage
