import React from 'react'
import Link from 'gatsby-link'

const ThirdPage = () => (
  <div>
    <h1>Hi from the second page</h1>
    <p>Welcome to page 3</p>
    <Link to="/page-4/">Go page 4</Link>
  </div>
)

export default ThirdPage
